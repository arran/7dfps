shader_type spatial;

uniform sampler2D tex: hint_albedo;
uniform sampler2D tex_normal: hint_normal;


uniform vec2 texture_scale = vec2(256.0, 128.0);

uniform float ground_level = 0.0;

uniform float max_flat_height = 2.0;
uniform float min_snow_height = 18.0;
uniform float slope_factor = 8.0;


uniform vec4 snow_col : hint_color;
uniform vec4 ground_col : hint_color;
uniform vec4 steep_col : hint_color;


varying float height_val;
varying vec3 normal;

void vertex(){
	height_val = VERTEX.y;
	normal = NORMAL;
}

float get_slope_of_terrain(float height_normal){
	float slope = 1.0-height_normal;
	slope *= slope;
	return (slope*slope_factor);
}

float snow_ground_mix(float curr_height){
	if (curr_height > min_snow_height){
		return 1.0;
	}
	if (curr_height  < max_flat_height){
		return 0.0;
	}
	return (curr_height - max_flat_height) / (min_snow_height - max_flat_height);
}

void fragment(){
	float snow_mix = clamp(snow_ground_mix(height_val), 0.0, 1.0);
	float slope = clamp(get_slope_of_terrain(normal.y), 0.0, 1.0);
	
	// Color the flat colour according to the steepness
	vec3 flat_mixin;
	if (height_val >= ground_level){
		flat_mixin = mix(ground_col.rgb, steep_col.rgb, slope);
	}
	else{
		flat_mixin = mix(ground_col.rgb, steep_col.rgb, slope*3.0);
	}
    
	vec3 ground_mixin = mix(ground_col.rgb, snow_col.rgb, slope);
	vec3 snow_mixin = mix(snow_col.rgb, ground_mixin, slope);

	vec3 mixin = mix(flat_mixin, snow_mixin, snow_mix);
    vec3 snow_texture = texture(tex, UV * texture_scale).rgb;
    
    mixin = mix(snow_texture, mixin, 0.5);
    
    float fresnel = sqrt(1.0 - dot(NORMAL, VIEW));
    ALBEDO = mixin + (0.1 * fresnel);
    ROUGHNESS = 0.05 * (1.0 - fresnel);
    METALLIC = 0.0;
    RIM = 0.1 * fresnel;
    NORMALMAP = texture(tex_normal, UV * texture_scale).rgb;
}





