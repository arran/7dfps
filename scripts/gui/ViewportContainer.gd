extends ViewportContainer

export (NodePath) var player_path : NodePath

func _process(delta):
    var player = get_node(player_path).find_node("Player") as Player
    get_material().set_shader_param("player_is_swimming", player.is_swimming())
