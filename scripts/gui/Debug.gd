extends Control

export (NodePath) var playerNode: NodePath
var player : KinematicBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
    player = get_node(playerNode)
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    var velocity =  player.get("velocity") as Vector3
    $Velocity.text = str(velocity.length())
    
    var target_velocity =  player.get("target_velocity") as Vector3
    $Target_Velocity.text = str(target_velocity.length())
