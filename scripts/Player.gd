extends KinematicBody
class_name Player

export (float, 0, 20, 0.1) var gravity: float = 9.8
export (float, 1, 5, 0.1) var look_sensitivity = 1

export (float, 1, 20, 0.5) var waddle_accel : float = 20
export (float, 1, 20, 0.5) var max_waddle_speed : float = 10

export (float, 0, 10, 0.5) var min_slide_accel : float = 5
export (float, 1, 40, 0.5) var max_slide_accel : float = 10
export (float, 20, 100, 0.5) var max_slide_speed : float = 40
var slide_accel

export (float, 1, 100, 0.5) var swim_accel: float = 30
export (float, 30, 100, 0.5) var max_swim_speed: float = 60

export (float, 1, 20, 0.5) var air_accel: float = 10
export (float, 0, 100, 0.5) var max_air_speed: float = 40

export (float, -30, 0, 0.005) var incline_slide_scalar : float = -25
export (float, 0, 30, 0.005) var decline_slide_scalar : float = 5

export (float, 1, 5, 0.25) var drag_scalar : float = 2

export (float, 5, 90, 0.5) var max_sprite_angle: float = 35

export (NodePath) var view : NodePath

var movement_dir : Vector3
var velocity : Vector3 = Vector3.ZERO
var target_velocity : Vector3 = Vector3.ZERO

enum states {
    AIR,
    WADDLE,
    SLIDE,
    SWIM
}

var previous_state = states.AIR
var current_state = states.WADDLE

var head_position
var spawn_position

func _ready():
    Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
    head_position = $Head.translation
    spawn_position = transform.origin

func _input(event):
    if event is InputEventMouseMotion:
        rotation_degrees.y -= event.relative.x * look_sensitivity / 10
        $Head.rotation_degrees.x = clamp($Head.rotation_degrees.x - event.relative.y * look_sensitivity / 10, -90, 90)

func _process(delta):
    
    # Quit game
    if Input.is_action_pressed("ui_cancel"):
        Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
        get_tree().quit(0)
        
    # Respawn
    if Input.is_action_just_pressed("move_respawn"):
        teleport_player(spawn_position)
    
    if current_state == states.WADDLE:
        if Input.is_action_pressed("move_slide") and velocity.length() >= (max_waddle_speed * 0.9):
            change_state(states.SLIDE)
            
    elif current_state == states.SLIDE:
        if not Input.is_action_pressed("move_slide") and velocity.length() <= (max_waddle_speed * 1.5):
            change_state(states.WADDLE)
            
    movement_dir = Vector3.ZERO
    movement_dir.z = -Input.get_action_strength("move_forward") + Input.get_action_strength("move_backward")
    movement_dir.x = -Input.get_action_strength("move_left") + Input.get_action_strength("move_right")
    movement_dir = movement_dir.normalized().rotated(Vector3.UP, rotation.y)
    
    update_view_sprite(delta)
    
func teleport_player(world_position: Vector3):
    spawn_position = world_position + (Vector3.UP * 10) + (-world_position.normalized() * 10)
    print("Moving Player to: %s" % spawn_position)
    transform.origin = spawn_position
    
func is_swimming():
    return current_state == states.SWIM
    
func change_state(new_state):
    previous_state = current_state
    current_state = new_state
    if new_state == states.SLIDE:
        slide_accel = max_slide_accel
        
    if previous_state == states.SWIM and new_state == states.AIR:
        target_velocity += (target_velocity * target_velocity.length() / 4 )
        
    print("Moved from %s to %s" % [states.keys()[previous_state], states.keys()[current_state]])
    

func get_movement_forces(delta):
    match(current_state):        
        states.WADDLE:
            var force = movement_dir * waddle_accel
            if is_on_floor():
                return adjust_vector_by_normal(force, get_floor_normal())
            return force
            
        states.SLIDE:
            var velocity_dir = velocity.normalized()
            var dot = velocity_dir.dot(-transform.basis.y)

            var variable_min_slide_accel = min_slide_accel
            
            # Scalar to decrease/increase up or slide acceleration
            # negative is decrease speed / positive is to increase
            var incline_decline_scalar
            
            # Sliding DOWN
            if dot > 0:
                incline_decline_scalar = decline_slide_scalar
            # Sliding UP
            elif dot < 0:
                incline_decline_scalar = incline_slide_scalar
                variable_min_slide_accel = 0
            # Sliding FLAT
            else:
                incline_decline_scalar = incline_slide_scalar
            
            slide_accel += (incline_decline_scalar * (1-dot * -1)) * delta
            slide_accel = clamp(slide_accel, variable_min_slide_accel, max_slide_accel)
            
            var force = (-transform.basis.z * slide_accel)
            if is_on_floor():
                return force
                return adjust_vector_by_normal(force, get_floor_normal())
            return force
            
        states.SWIM:
            return(-$Head.global_transform.basis.z * swim_accel)
            
        states.AIR:
            return (-transform.basis.z * air_accel)

func _physics_process(delta):
    
    # Swimming hack
    if global_transform.origin.y < 0 and current_state != states.SWIM:
        change_state(states.SWIM)
    elif global_transform.origin.y > 0 and current_state == states.SWIM:
        change_state(states.AIR)
    
    # If coming out of water, slide
    if previous_state == states.SWIM and current_state == states.AIR and is_on_floor():
        change_state(states.SLIDE)

    var movement_force = get_movement_forces(delta)
    target_velocity += movement_force * delta
    
    # Cap Target Velocity
    target_velocity = get_clamped_velocity(target_velocity)
    
    # Apply drag
    var dot = movement_force.normalized().dot(target_velocity.normalized())
    var drag = (1-dot) * 2 * delta
    target_velocity -= target_velocity * drag

    # apply gravity when not on floor or when swimming
    var gravity_force = (Vector3.DOWN * pow(gravity,2))
    if not is_on_floor() and current_state != states.SWIM:
        target_velocity += gravity_force * delta
        
    # Adjust Velocity
    velocity = move_and_slide(target_velocity, Vector3.UP, true, 4, deg2rad(70), false)

func get_clamped_velocity(t_velocity):
    var max_speed
    match(current_state):
        states.WADDLE:
            max_speed = max_waddle_speed
        states.SLIDE:
            max_speed = max_slide_speed
        states.SWIM:
            max_speed = max_swim_speed
        states.AIR:
            max_speed = max_air_speed

    if t_velocity.length() > max_speed:
        return t_velocity.normalized() * max_speed
    else:
        return t_velocity

func adjust_vector_by_normal(vector: Vector3, normal: Vector3):
    return vector.cross(normal).rotated(normal.normalized(), deg2rad(90))

func update_view_sprite(delta):
    var view_rect = get_node(view) as TextureRect
    
    if current_state == states.WADDLE:
        view_rect.texture = load("res://sprites/arms.png")
    elif current_state == states.SLIDE:
        view_rect.texture = load("res://sprites/arms_slide.png")
    
    var view_scalar = 0
    match(current_state):
        states.SLIDE:
            view_scalar = 1.5
        states.WADDLE:
            view_scalar = 1
        states.SWIM:
            view_scalar = 1.5
        
    rotate_view_sprite(delta, view_scalar)

func rotate_view_sprite(delta, scalar):
    var view_rect = get_node(view) as TextureRect
    # The Velocity crossed with the player's forward vector
    # to determinate how sharp the player to steering left or right
    var velocity_forward_cross = target_velocity.cross(transform.basis.z)
    # Inverse for LEFT being negative
    var steering = velocity_forward_cross.y * scalar
    var clamped_steering = clamp(steering, -max_sprite_angle, max_sprite_angle)
    
    # Set the pivot offset at runtime to account for dyanmic resolution changes
    view_rect.rect_pivot_offset = view_rect.rect_size / 2
    view_rect.rect_rotation = lerp(view_rect.rect_rotation, clamped_steering, delta * 20)
