tool
extends Spatial
class_name Terrain

export (Material) var land_material : Material
export (Material) var water_material : Material
export (OpenSimplexNoise) var noise_large : OpenSimplexNoise
export (OpenSimplexNoise) var noise_medium : OpenSimplexNoise
export (OpenSimplexNoise) var noise_small : OpenSimplexNoise
export (Vector2) var terrain_size : Vector2 = Vector2(400,400)
export (float, 0.1, 1, 0.1) var terrain_subdivision : float = 0.1
export (int) var terrain_height : int = 30

export (float, 0.1, 1) var clamp_peak : float = 1
export (float, -1, 0.1) var clamp_valley : float = -1

export (float) var land_height_offset : float = 0

var peak_world_location = Vector3.ZERO

signal finished_generation

export (bool) var preview_in_editor = false
var thread : Thread

func _process(delta):
    if preview_in_editor and Engine.editor_hint:
        generate_terrain(null)
        preview_in_editor = false

func _ready():
    if not Engine.editor_hint:
        thread = Thread.new()
        var id = thread.start(self, "generate_terrain")
        print("Thread [ %s ] Generating Terrain" % id)

func _exit_tree():
    if thread.is_active():
        thread.wait_to_finish()
        
# --- Generation --- 

func generate_terrain(args):
    remove_existing_meshes()
    var heightmap_mesh = generate_heightmap_mesh()
    var water_mesh = generate_water_mesh()
    
    var water_area = area_from_mesh(water_mesh)
    water_area.connect("body_entered", self, "on_water_entered")
    water_area.connect("body_exited", self, "on_water_exited")
    
    add_child(heightmap_mesh)
    add_child(water_area)    
    
    if not Engine.editor_hint:
        emit_signal("finished_generation", peak_world_location)

func area_from_mesh(mesh : MeshInstance) -> Area:
    var water_area = Area.new()
    var water_collision = CollisionShape.new()
    water_area.add_child(water_collision)
    
    water_area.add_child(mesh)
    water_collision.make_convex_from_brothers()
    water_collision.transform = mesh.transform
    return water_area


func remove_existing_meshes():
    if get_child_count() > 0:
        var children = get_children()
        for child in children:
            remove_child(child)

func generate_heightmap_mesh() -> MeshInstance:
    var plane_mesh = PlaneMesh.new()
    plane_mesh.size = terrain_size
    var depth = round(terrain_size.y * terrain_subdivision)
    var width = round(terrain_size.x * terrain_subdivision) 
    plane_mesh.subdivide_depth = depth
    plane_mesh.subdivide_width = width
    
    var surface_tool = SurfaceTool.new()
    surface_tool.create_from(plane_mesh, 0)
    var array_plane = surface_tool.commit()
    
    var data_tool = MeshDataTool.new()
    data_tool.create_from_surface(array_plane, 0)
    
    var vertex_count = data_tool.get_vertex_count()
    for i in range(vertex_count):
        var vertex = data_tool.get_vertex(i)
        vertex.y = get_vertex_height(data_tool, i) * terrain_height + land_height_offset
        data_tool.set_vertex(i, vertex)

        if vertex.y > peak_world_location.y:
            peak_world_location = vertex

    var surface_count = array_plane.get_surface_count()
    for i in range(surface_count):
        array_plane.surface_remove(i)
    
    data_tool.commit_to_surface(array_plane)
    
    surface_tool.begin(Mesh.PRIMITIVE_TRIANGLES)
    surface_tool.create_from(array_plane, 0)
    surface_tool.generate_normals()
        
    var mesh_instance = MeshInstance.new()
    mesh_instance.mesh = surface_tool.commit()
    mesh_instance.set_surface_material(0, land_material)
    mesh_instance.create_trimesh_collision()
    
    return mesh_instance


func get_vertex_height(data_tool : MeshDataTool, index: int) -> float:
    var vertex = data_tool.get_vertex(index)
    
    var large_noise = noise_large.get_noise_3dv(vertex)    
    var medium_noise = noise_medium.get_noise_3dv(vertex)
    var small_noise = noise_small.get_noise_3dv(vertex)
    
    var vertex_height = (large_noise) + (medium_noise * 0.3) + (small_noise * 0.125)
    
    if vertex_height < clamp_valley:
        vertex_height = clamp_valley
    elif vertex_height > clamp_peak:
        vertex_height = clamp_peak

    return vertex_height    
    
func generate_water_mesh() -> MeshInstance:
    var mesh_instance = MeshInstance.new()
    var cube_mesh  = CubeMesh.new()
    cube_mesh.size = Vector3(terrain_size.x, terrain_height, terrain_size.y)
    mesh_instance.mesh = cube_mesh
    mesh_instance.set_surface_material(0, water_material)
    mesh_instance.transform.origin = Vector3.DOWN * ( terrain_height / 2)
    
    return mesh_instance
